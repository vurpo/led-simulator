extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
extern crate serial;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };
use std::io;
use std::io::*;
use serial::prelude::*;

pub struct App {
  gl: GlGraphics,
  parser: CmdParser,
  serial_port: serial::SystemPort,
}

#[derive(Copy, Clone, Debug)]
pub struct Led {
  r: u8,
  g: u8,
  b: u8
}

pub struct CmdParser {
  command: Vec<(u8,u8,u8)>,
  color: [u8; 3],
  color_counter: u8,
  byte_counter: u8,
}

impl CmdParser {
  fn parse(&mut self, c: u8) -> Option<Vec<(u8, u8, u8)>> {
    let mut c = c;
    if c&0b10000000 == 0b10000000 {
      self.byte_counter = 0;
      self.color_counter = 0;
      c = c & 0b01111111;
    }

    self.color[self.color_counter as usize] = c;
    
    self.color_counter += 1;
    self.byte_counter += 1;

    if self.color_counter >= 3 {
      self.color_counter = 0;
      self.command.push((self.color[0], self.color[1], self.color[2]));
    }

    if self.byte_counter >= 150 {
      self.byte_counter = 0; 
      self.color_counter = 0;

      let return_vec = self.command.to_owned();
      self.command = Vec::new();

      Some(return_vec)
    } else {
      None
    }
  }
}


impl App {
  fn render(&mut self, leds: &[Led], args: &RenderArgs) {
    use graphics::*;

    const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

    let square = rectangle::square(0.0, 0.0, 20.0);

    self.gl.draw(args.viewport(), |c, gl| {
      clear(BLACK, gl);

      for (i, led) in leds.iter().enumerate() {
        ellipse([(led.r as f32)/127.0, (led.g as f32)/127.0, (led.b as f32)/127.0, 1.0],
          square, c.transform.trans((i as f64)*20 as f64, 0 as f64), gl);
      }
    });
  }

  fn update(&mut self, leds: &mut [Led], args: &UpdateArgs) {
    let mut buffer = [0u8; 1];
    match self.serial_port.read(&mut buffer) {
      Ok(_) => {
        for x in buffer.iter() {
          if let Some(strip) = self.parser.parse(*x) {
            //println!("DEBUG: LED {} old value {},{},{}, new value {},{},{}", n, leds[n as usize].r, leds[n as usize].g, leds[n as usize].b, r, g, b);
            //leds[n as usize].r = r;
            //leds[n as usize].g = g;
            //leds[n as usize].b = b;

            for (i,led) in strip.iter().enumerate().take(50) {
              leds[i as usize].r = led.0;
              leds[i as usize].g = led.1;
              leds[i as usize].b = led.2;
            }
            self.serial_port.write(&[0]); //we are ready to receive
          }
        }
      },
      Err(_) => {
        println!("{}", "Error reading stdin!");
      }
    }
  }
}

fn main() {
  let opengl = OpenGL::V3_2;
  let mut window: Window = WindowSettings::new(
      "LED SIMULATOR 2016",
      [20*50, 20],
    )
    .opengl(opengl)
    .exit_on_esc(true)
    .build()
    .unwrap();
  
  let mut leds = [Led{r:0,g:0,b:0}; 50];

  let mut app = App {
    gl: GlGraphics::new(opengl),
    parser: CmdParser {command: Vec::new(), color: [0u8;3], color_counter: 0, byte_counter: 0},
    serial_port: {
      let mut  s = serial::open("/dev/pts/5").unwrap();
      s.reconfigure(&|conf| {
        try!(conf.set_baud_rate(serial::Baud115200));
        Ok(())
      }).unwrap();
      s
    }
  };

  let mut events = window.events();
  events.set_ups(3000);
  events.set_max_fps(120);

  app.serial_port.write(&[0]); //we are ready

  while let Some(e) = events.next(&mut window) {
    if let Some(r) = e.render_args() {
      app.render(&leds, &r);
    }

    if let Some(u) = e.update_args() {
      app.update(&mut leds, &u);
    }
  }
}
